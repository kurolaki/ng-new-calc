import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { CalcServiceService } from './calc-service.service';
import { CalcScreenComponent } from './calc-screen/calc-screen.component';
import { CalcButtonsComponent } from './calc-buttons/calc-buttons.component';

@NgModule({
  declarations: [AppComponent, CalcScreenComponent, CalcButtonsComponent],
  imports: [BrowserModule, FormsModule],
  providers: [CalcServiceService],
  bootstrap: [AppComponent]
})
export class AppModule {}
