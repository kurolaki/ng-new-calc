import { Component, OnInit } from '@angular/core';
import { CalcServiceService } from '../calc-service.service';

@Component({
  selector: 'app-calc-buttons',
  templateUrl: './calc-buttons.component.html',
  styleUrls: ['./calc-buttons.component.scss']
})
export class CalcButtonsComponent implements OnInit {
  constructor(private calcService: CalcServiceService) {}

  ngOnInit() {}

  public onClick(val: string) {
    this.calcService.setCurrentValue(val);
    console.log(val);
  }

  public isVisible(val: string): boolean {
    return this.calcService.isVisible(val);
  }
}
