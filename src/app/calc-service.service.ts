import { Injectable } from '@angular/core';

@Injectable()
export class CalcServiceService {
  currentValue = '0';
  buttons = {
    equal: true,
    plus: false,
    minus: false,
    divide: false,
    percent: true,
    multiply: false,
    dot: false,
    clear: true,
    sqrt: true,
    power: true
  };

  visible = ['plus', 'dot', 'multiply', 'divide', 'minus'];

  constructor() {}

  setCurrentValue(val: string) {
    if (this.currentValue !== '0') {
      this.currentValue = `${this.currentValue}${val}`;
      if (this.visible.indexOf(val) !== -1) {
        this.buttons[val] = true;
        console.log(this.buttons);
      }
    } else {
      this.currentValue = val;
    }
  }

  getCurrentValue(): string {
    return this.currentValue;
  }

  isVisible(val) {
    return this.buttons[val];
  }
}
