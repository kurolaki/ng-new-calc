import { Component, OnInit } from '@angular/core';
import { CalcServiceService } from '../calc-service.service';

@Component({
  selector: 'app-calc-screen',
  templateUrl: './calc-screen.component.html',
  styleUrls: ['./calc-screen.component.scss']
})
export class CalcScreenComponent implements OnInit {
  screenValue = this.calcService.getCurrentValue;
  equation = '3231+3212';
  constructor(private calcService: CalcServiceService) {}

  private getValue(): string {
    return this.calcService.getCurrentValue();
  }

  ngOnInit() {}
}
