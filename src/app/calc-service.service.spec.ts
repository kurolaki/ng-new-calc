import { TestBed, inject } from '@angular/core/testing';

import { CalcServiceService } from './calc-service.service';

describe('CalcServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CalcServiceService]
    });
  });

  it('should be created', inject([CalcServiceService], (service: CalcServiceService) => {
    expect(service).toBeTruthy();
  }));
});
